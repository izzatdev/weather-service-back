Weather-Service-back Проект
===============================

### Получить проект из VCS

```
cd existing_repo
git remote add origin https://gitlab.com/izzatdev/weather-service-back.git
git branch -M main
git push -uf origin main
```

### Необходимые ссылки

* #### <a href="http://localhost:8081/swagger-ui/index.html#/">Сваггер-линк</a>
* #### <a href="description.md">Описание и план проекта

![first](docs/first.jpg)

    За исключением register и login, все endpoints требуют authentication.
    Итак, вы должны сначала register или login.
    Все зарегистрированные пользователи по умолчанию имеют роль 'User'.
    В системе по умолчанию есть один admin с учетными данными (username: test, password: test).

    @Author: Izzatulla. 04.05.2023

![second](docs/second.jpg)

    После регистрации токена вставьте его во всплывающее окно Authenticated.
    Не забудьте добавить предисловие «Bearer». И всё.
    Вуаля, Use and Enjoy

    @Author: Izzatulla. 04.05.2023
