package com.atmos.weatherservicedemo.controllers;

import com.atmos.weatherservicedemo.domain.entities.City;
import com.atmos.weatherservicedemo.domain.entities.User;
import com.atmos.weatherservicedemo.mappers.CityMapper;
import com.atmos.weatherservicedemo.services.CityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atmos.weatherservicedemo.domain.entities.City.Type.CELSIUS;
import static com.atmos.weatherservicedemo.domain.entities.User.Role.ROLE_ADMIN;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CityController.class)
class CityControllerTest {
    @MockBean
    CityMapper cityMapper;
    @MockBean
    private CityService cityService;

    @Autowired
    private WebTestClient webClient;


    @Test
    void getSubs() {
        var tokenString = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoiUk9MRV9BRE1JTiIsInN1YiI6InRlc3QiLCJpYXQiOjE2ODMyMTA2OTUsImV4cCI6MTk3MTIxMDY5NX0.MqAWDxoBMKNdqP2-_ojQKJazs8nCRacnaRCVGyDl_2Lev6-URsYmGhbe3ns54Unv2ygW8c38ROLyR_senjK1HA";

        User user = new User(Collections.EMPTY_SET, 1, "test", "test", true, ROLE_ADMIN);
        Set<City> cities = new HashSet<>();
        cities.add(new City(Collections.EMPTY_SET, 1, "name", CELSIUS, 21, "desc", true, LocalDateTime.now()));
        cities.add(new City(Collections.EMPTY_SET, 2, "name", CELSIUS, 21, "desc", true, LocalDateTime.now()));
        cities.add(new City(Collections.EMPTY_SET, 3, "name", CELSIUS, 21, "desc", false, LocalDateTime.now()));
        cities.add(new City(Collections.EMPTY_SET, 4, "name", CELSIUS, 21, "desc", false, LocalDateTime.now()));
        cities.add(new City(Collections.EMPTY_SET, 5, "name", CELSIUS, 21, "desc", true, LocalDateTime.now()));


        Mockito.when(cityService.findSubscribedCities(user.getId())).thenReturn(Flux.fromIterable(cities.stream().filter(City::isVisible).collect(Collectors.toSet())));
        Flux<City> cityFlux = cityService.findSubscribedCities(user.getId());

        StepVerifier
                .create(cityFlux)
                .expectNextCount(3)
                .expectComplete()
                .verify();

    }
}