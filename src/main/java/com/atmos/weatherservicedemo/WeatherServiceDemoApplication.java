package com.atmos.weatherservicedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.r2dbc.config.EnableR2dbcAuditing;

/**
 * WeatherServiceDemoApplication class
 *
 * @author Izzatulla toshpulatov
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class WeatherServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherServiceDemoApplication.class, args);
    }


}
