package com.atmos.weatherservicedemo.dao;

import com.atmos.weatherservicedemo.domain.entities.UserCity;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCityDao extends ReactiveSortingRepository<UserCity, Integer> {

}
