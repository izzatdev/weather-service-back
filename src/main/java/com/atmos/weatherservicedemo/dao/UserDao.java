package com.atmos.weatherservicedemo.dao;

import com.atmos.weatherservicedemo.domain.entities.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserDao extends ReactiveSortingRepository<User, Integer> {
    Mono<User> findByUsername(String username);

    Flux<User> findAll(Sort sort);
}
