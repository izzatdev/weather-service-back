package com.atmos.weatherservicedemo.dao;

import com.atmos.weatherservicedemo.domain.entities.City;
import org.springframework.data.domain.Sort;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface CityDao extends ReactiveSortingRepository<City, Integer> {
    Flux<City> findCitiesByVisibleIsTrue(Sort sort);

    Flux<City> findCitiesByVisibleIsFalse(Sort sort);

    Flux<City> findAll(Sort sort);

    Mono<Boolean> existsByName(String name);

    @Query("select DISTINCT c.* from cities  c inner join subscribed_cities sc on c.id = sc.city_id where sc.user_id=:id order by c.name")
    Flux<City> findCitiesByUserId(Integer id);
}
