package com.atmos.weatherservicedemo.services;

import com.atmos.weatherservicedemo.domain.entities.City;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CityService {
    /**
     * Create a new city
     *
     * @param city City to be created
     * @return the saved city without the related entities
     */
    Mono<City> create(City city);


    /**
     * Find a city
     *
     * @param id identifier of the item
     * @return the city
     */
    Mono<City> findById(Integer id);

    /**
     * Update a city
     *
     * @param city to be saved
     * @return the saved city without the related entities
     */
    Mono<City> update(City city);

    /**
     * Get All Cities
     *
     * @return Cities
     */
    Flux<City> findAll();

    /**
     * Get all city with filter
     *
     * @param visible to filter the cities via isVisible property
     * @return the Cities
     */
    Flux<City> findAllByVisible(Boolean visible);

    /**
     * Get all subscribed cities
     *
     * @param userId identifier of user
     * @return the subscribed cities
     */
    Flux<City> findSubscribedCities(Integer userId);
}
