package com.atmos.weatherservicedemo.services;

import com.atmos.weatherservicedemo.domain.dtos.AuthResponseDto;
import com.atmos.weatherservicedemo.domain.dtos.UserDetailsDto;
import com.atmos.weatherservicedemo.domain.entities.City;
import com.atmos.weatherservicedemo.domain.entities.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    /**
     * Register a user
     *
     * @param user to save
     * @return the AuthResponse with jwt token
     */
    Mono<AuthResponseDto> register(User user);

    /**
     * Sign in
     *
     * @param user to authenticate
     * @return the AuthResponse with jwt token
     */
    Mono<AuthResponseDto> authenticate(User user);

    /**
     * Get All users
     *
     * @return Users
     */
    Flux<User> getAll();

    /**
     * Get exact user
     *
     * @param id to get by id
     * @return the UserDetailsDto
     */
    Mono<UserDetailsDto> getById(Integer id);

    /**
     * Edit exact user
     *
     * @param user to edit
     * @return the updated User
     */
    Mono<User> edit(User user);

    /**
     * Subscribe to the target city
     *
     * @param userId . the id of user who is subscribing
     * @param city . the target city
     * @return the UserDetailsDto
     */
    Mono<UserDetailsDto> subscribe(Integer userId, City city);
}
