package com.atmos.weatherservicedemo.services.impl;

import com.atmos.weatherservicedemo.dao.CityDao;
import com.atmos.weatherservicedemo.domain.entities.City;
import com.atmos.weatherservicedemo.exceptions.CityNotFoundException;
import com.atmos.weatherservicedemo.services.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.atmos.weatherservicedemo.utils.Utils.DEFAULT_SORT;

@Service
@Transactional
@RequiredArgsConstructor
public class CityServiceImpl implements CityService {
    @Autowired
    private CityDao cityDao;

    @Override
    public Mono<City> create(City city) {
        if (city.getId() != null || city.getType() == null) {
            throw new IllegalArgumentException("When creating a city, the id must be null, type mustn't be null");
        }

        city.updateDateTime();
        city.setVisible(true);

        return verifyExistence(city.getName()).then(cityDao.save(city));
    }

    @Override
    public Mono<City> findById(Integer id) {
        return cityDao.findById(id)
                .switchIfEmpty(Mono.error(new CityNotFoundException(id)));
    }

    @Override
    public Mono<City> update(City city) {

        if (city.getId() == null || city.getType() == null) {
            throw new IllegalArgumentException("When updating a city, the id and type mustn't be null");
        }
        city.updateDateTime();
        return verifyExistence(city.getId()).then(cityDao.save(city));
    }

    @Override
    public Flux<City> findAll() {
        return cityDao.findAll(DEFAULT_SORT);
    }

    @Override
    public Flux<City> findAllByVisible(Boolean visible) {
        return visible ? cityDao.findCitiesByVisibleIsTrue(DEFAULT_SORT) : cityDao.findCitiesByVisibleIsFalse(DEFAULT_SORT);
    }

    @Override
    public Flux<City> findSubscribedCities(Integer userId) {
        return cityDao.findCitiesByUserId(userId).filter(City::isVisible);

    }

    private Mono<Boolean> verifyExistence(Integer id) {
        return cityDao.existsById(id).handle((exists, sink) -> {
            if (Boolean.FALSE.equals(exists)) {
                sink.error(new CityNotFoundException(id));
            } else {
                sink.next(exists);
            }
        });
    }

    private Mono<Boolean> verifyExistence(String name) {
        return cityDao.existsByName(name).handle((exists, sink) -> {
            if (Boolean.TRUE.equals(exists)) {
                sink.error(new IllegalArgumentException("This city already has been added: " + name));
            } else {
                sink.next(exists);
            }
        });
    }
}
