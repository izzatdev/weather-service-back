package com.atmos.weatherservicedemo.services.impl;

import com.atmos.weatherservicedemo.dao.CityDao;
import com.atmos.weatherservicedemo.dao.UserCityDao;
import com.atmos.weatherservicedemo.dao.UserDao;
import com.atmos.weatherservicedemo.domain.dtos.AuthResponseDto;
import com.atmos.weatherservicedemo.domain.dtos.UserDetailsDto;
import com.atmos.weatherservicedemo.domain.entities.City;
import com.atmos.weatherservicedemo.domain.entities.User;
import com.atmos.weatherservicedemo.domain.entities.UserCity;
import com.atmos.weatherservicedemo.exceptions.UserNotFoundException;
import com.atmos.weatherservicedemo.mappers.UserMapper;
import com.atmos.weatherservicedemo.security.JwtUtil;
import com.atmos.weatherservicedemo.security.PBKDF2Encoder;
import com.atmos.weatherservicedemo.services.UserService;
import com.atmos.weatherservicedemo.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final CityDao cityDao;

    private final UserMapper userMapper;
    private final UserCityDao ucDao;
    private final PBKDF2Encoder passwordEncoder;
    private final JwtUtil jwtUtil;

    @Override
    public Mono<User> edit(User user) {
        if (user.getId() == null || user.getUsername() == null) {
            throw new IllegalArgumentException("When updating a user, the id and username mustn't be null");
        }
        return verifyExistence(user.getId())
                .then(userDao.findById(user.getId()))
                .map(resource -> {
                    user.setSubscribedCities(resource.getSubscribedCities());
                    user.setEnabled(resource.isEnabled());
                    user.setPassword(resource.getPassword());
                    return user;
                })
                .then(userDao.save(user));
    }

    @Override
    public Mono<UserDetailsDto> subscribe(Integer userId, City city) {
        if (userId == null) {
            return Mono.error(new IllegalArgumentException("While subscribe, the user id mustn't be null"));
        }
        UserCity uc = new UserCity();
        uc.setUserId(userId);
        uc.setCityId(city.getId());
        return verifyExistence(userId)
                .then(ucDao.save(uc))
                .flatMap(userCity -> {
                    return userDao.findById(userCity.getUserId()).flatMap(user -> {
                        return userDao.findById(user.getId()).flatMap(this::loadRelations);
                    });
                });
    }

    @Override
    public Mono<AuthResponseDto> register(User user) {
        if (user.getId() != null) {
            return Mono.error(new IllegalArgumentException("While user register, the id must be null"));
        }
        user.setRole(User.Role.ROLE_USER);
        user.setEnabled(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userDao.save(user)
                .map(savedUser -> new AuthResponseDto(jwtUtil.generateToken(savedUser), "success", Utils.getCurrentTimeInString()))
                .switchIfEmpty(Mono.just(new AuthResponseDto(null, "Wrong credentials", null)));
    }

    @Override
    public Mono<AuthResponseDto> authenticate(User user) {
        if (user.getUsername() == null || user.getPassword() == null) {
            throw new IllegalArgumentException("While authenticate credentials mustn't be null");
        }

        return userDao.findByUsername(user.getUsername())
                .filter(userDetails -> passwordEncoder.matches(user.getPassword(), userDetails.getPassword()))
                .map(userDetails -> new AuthResponseDto(jwtUtil.generateToken(userDetails), "success", Utils.getCurrentTimeInString()))
                .switchIfEmpty(Mono.just(new AuthResponseDto(null, "Wrong credentials", null)));

    }

    @Override
    public Flux<User> getAll() {
        return userDao.findAll(Sort.by("username"));
    }

    @Override
    public Mono<UserDetailsDto> getById(Integer id) {
        return userDao.findById(id)
                .switchIfEmpty(Mono.error(new UserNotFoundException(id)))
                .flatMap(this::loadRelations);
    }

    private Mono<Boolean> verifyExistence(Integer id) {
        return userDao.existsById(id).handle((exists, sink) -> {
            if (Boolean.FALSE.equals(exists)) {
                sink.error(new UserNotFoundException(id));
            } else {
                sink.next(exists);
            }
        });
    }

    private Mono<UserDetailsDto> loadRelations(final User user) {
        Flux<City> cities = cityDao.findCitiesByUserId(user.getId());
        // Load the cities
        return Mono.just(user)
                .zipWith(cities.collect(Collectors.toSet()))
                .map(objects -> {
                    objects.getT1().setSubscribedCities(objects.getT2());
                    return userMapper.toDetails(objects.getT1());
                });
    }

}
