package com.atmos.weatherservicedemo.exceptions;

public class UserNotFoundException extends NotFoundException {

    public UserNotFoundException(Integer id) {
        super(String.format("Item [%d] is not found", id));
    }
}
