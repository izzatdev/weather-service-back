package com.atmos.weatherservicedemo.exceptions;

public class CityNotFoundException extends NotFoundException {
    public CityNotFoundException(Integer id) {
        super(String.format("Item [%d] is not found", id));
    }
}
