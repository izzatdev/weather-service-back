package com.atmos.weatherservicedemo.domain.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class CityAddDto {
    @NotBlank
    private String name;

    @NotNull
    private char type;

    @NotNull
    private int weather;

    @NotBlank
    @Size(max = 4000)
    private String description;
}
