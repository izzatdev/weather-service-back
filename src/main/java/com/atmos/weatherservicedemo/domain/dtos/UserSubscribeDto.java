package com.atmos.weatherservicedemo.domain.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserSubscribeDto {
    private Integer userId;
    private Integer cityId;
}
