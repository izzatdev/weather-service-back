package com.atmos.weatherservicedemo.domain.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
public class UserDetailsDto {
    private String id;
    private String username;
    private String role;
    private Set<String> subscriptions;
}
