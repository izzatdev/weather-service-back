package com.atmos.weatherservicedemo.domain.dtos;

import com.atmos.weatherservicedemo.domain.entities.City;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CityShowDto {
    private int id;
    private String weather;
    private String name;
    private String description;
    private String LocalDateTime;


    public CityShowDto(City city) {
        this.id = city.getId();
        this.weather = city.weatherString();
        this.description = city.getDescription();
        this.LocalDateTime = city.dateTimeString();
        this.name = city.getName();
    }
}
