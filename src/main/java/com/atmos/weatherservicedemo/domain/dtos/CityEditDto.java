package com.atmos.weatherservicedemo.domain.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CityEditDto {
    @NotBlank
    private String name;

    @NotNull
    private boolean visibility;

}
