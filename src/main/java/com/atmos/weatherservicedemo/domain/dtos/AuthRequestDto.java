package com.atmos.weatherservicedemo.domain.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class AuthRequestDto {
    @NotNull
    private String username;

    @NotNull
    private String password;
}
