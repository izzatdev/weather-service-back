package com.atmos.weatherservicedemo.domain.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class AuthResponseDto {
    private String token;
    private String message;
    private String issuedAt;
}

