package com.atmos.weatherservicedemo.domain.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDto {
    private Integer id;
    private String username;
    private String role;
}
