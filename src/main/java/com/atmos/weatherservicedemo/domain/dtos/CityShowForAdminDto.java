package com.atmos.weatherservicedemo.domain.dtos;

import com.atmos.weatherservicedemo.domain.entities.City;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CityShowForAdminDto extends CityShowDto {

    private boolean isVisible;

    public CityShowForAdminDto(City city) {
        super(city);
        this.isVisible = city.isVisible();
    }
}
