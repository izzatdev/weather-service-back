package com.atmos.weatherservicedemo.domain.entities;

import com.atmos.weatherservicedemo.utils.Utils;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table("cities")
public class City {
    @Transient
    Set<User> users;

    @Id
    private Integer id;

    private String name;


    private Type type;

    private int weather;
    private String description;
    private boolean visible;
    @LastModifiedDate
    private LocalDateTime localDateTime;

    public void updateDateTime() {
        this.localDateTime = LocalDateTime.now();
    }

    public String weatherString() {
        return weather + type.shortName;
    }

    public String dateTimeString() {
        return localDateTime.format(DateTimeFormatter.ofPattern(Utils.dateTimePattern));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return weather == city.weather && visible == city.visible && Objects.equals(users, city.users) && Objects.equals(id, city.id) && Objects.equals(name, city.name) && type == city.type && Objects.equals(description, city.description) && Objects.equals(localDateTime, city.localDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(users, id, name, type, weather, description, visible, localDateTime);
    }

    public enum Type {
        CELSIUS(" ℃"), FAHRENHEIT(" ℉");

        private final String shortName;

        Type(String shortName) {
            this.shortName = shortName;
        }

        public String getShortName() {
            return this.shortName;
        }
    }
}
