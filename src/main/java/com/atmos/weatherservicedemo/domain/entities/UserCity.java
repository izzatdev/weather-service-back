package com.atmos.weatherservicedemo.domain.entities;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;

@Table("subscribed_cities")
@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserCity {
    @Id
    private Integer id;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer cityId;

}
