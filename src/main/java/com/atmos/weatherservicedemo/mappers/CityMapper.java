package com.atmos.weatherservicedemo.mappers;

import com.atmos.weatherservicedemo.domain.dtos.CityAddDto;
import com.atmos.weatherservicedemo.domain.dtos.CityEditDto;
import com.atmos.weatherservicedemo.domain.dtos.CityUpdateDto;
import com.atmos.weatherservicedemo.domain.entities.City;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import static com.atmos.weatherservicedemo.domain.entities.City.Type.CELSIUS;
import static com.atmos.weatherservicedemo.domain.entities.City.Type.FAHRENHEIT;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class CityMapper {

    public abstract City update(City city);

    @Mapping(target = "type", source = "type", qualifiedByName = "getType")
    public abstract City toModel(CityAddDto resource);

    @Mapping(target = "type", source = "type", qualifiedByName = "getType")
    public abstract City toModel(CityUpdateDto resource);

    public abstract City toModel(CityEditDto resource);

    @Named("getType")
    public City.Type getType(Character ch) {
        if (ch == 'C') {
            return CELSIUS;
        }
        if (ch == 'F') {
            return FAHRENHEIT;
        }
        return null;
    }
}
