package com.atmos.weatherservicedemo.mappers;

import com.atmos.weatherservicedemo.domain.dtos.AuthRequestDto;
import com.atmos.weatherservicedemo.domain.dtos.UserDetailsDto;
import com.atmos.weatherservicedemo.domain.dtos.UserDto;
import com.atmos.weatherservicedemo.domain.entities.City;
import com.atmos.weatherservicedemo.domain.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class UserMapper {

    @Mapping(target = "role", source = "role", qualifiedByName = "getRoleName")
    @Mapping(target = "subscriptions", source = "subscribedCities", qualifiedByName = "subscribedCities")
    public abstract UserDetailsDto toDetails(User user);

    @Mapping(target = "role", source = "role", qualifiedByName = "getRoleName")
    public abstract UserDto toDto(User user);

    public abstract User toModel(AuthRequestDto request);

    @Mapping(target = "role", source = "role", qualifiedByName = "getRole")
    public abstract User toModel(UserDto userShowDto);


    @Named("getRoleName")
    String getRoleName(User.Role role) {
        return role.name();
    }

    @Named("getRole")
    User.Role getRole(String roleName) {
        return User.Role.valueOf(roleName);
    }

    @Named("subscribedCities")
    Set<String> subscribedCities(Set<City> cities) {
        return Objects.isNull(cities) ?
                Collections.EMPTY_SET :
                cities
                        .stream()
                        .map(City::getName)
                        .collect(Collectors.toSet());
    }


}
