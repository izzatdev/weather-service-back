package com.atmos.weatherservicedemo.controllers;

import com.atmos.weatherservicedemo.domain.dtos.*;
import com.atmos.weatherservicedemo.mappers.CityMapper;
import com.atmos.weatherservicedemo.services.CityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;
import static org.springframework.http.ResponseEntity.noContent;

@RestController
@RequestMapping(value = "/api/city-controller")
@RequiredArgsConstructor
@Slf4j
public class CityController {
    private final CityMapper cityMapper;
    private final CityService cityService;

    @ApiOperation(value = "Add a new City (Cities' name are unique). {access= Admin Role}",
            notes = "For \'type\' field you should input F or C character. (CELSIUS/FAHRENHEIT)",
            authorizations = {@Authorization(value = "JWT")})
    @PostMapping("/add-city")
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<ResponseEntity<Void>> create(@Valid @RequestBody final CityAddDto resource) {
        return cityService.create(cityMapper.toModel(resource))
                .map(city -> ResponseEntity.created(linkTo(CityController.class).slash("get").slash(city.getId()).toUri()).build());
    }

    @ApiOperation(value = "Find a city by its id. {access= Any Role}", authorizations = {@Authorization(value = "JWT")})
    @GetMapping(value = "/get/{id}", produces = {APPLICATION_JSON_VALUE})
    public Mono<CityShowDto> findById(@PathVariable final Integer id) {
        return cityService.findById(id).map(CityShowDto::new);
    }

    @ApiOperation(value = "Edit a City. {access= Admin Role}", authorizations = {@Authorization(value = "JWT")})
    @PutMapping(value = "/edit-city/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<ResponseEntity<Void>> EditCity(@PathVariable @NotNull final Integer id,
                                               @Valid @RequestBody final CityEditDto cityEditDto) {
        // Find the item and update the instance
        return cityService.findById(id)
                .map(item -> cityMapper.toModel(cityEditDto))
                .flatMap(cityService::update)
                .map(item -> noContent().build());
    }

    @ApiOperation(value = "Update city Weather. {access= Admin Role}", notes = "For \'type\' field you should input F or C character. (CELSIUS/FAHRENHEIT)", authorizations = {@Authorization(value = "JWT")})
    @PutMapping(value = "/update-city-weather/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<ResponseEntity<Void>> UpdateCityWeather(@PathVariable @NotNull final Integer id,
                                                        @Valid @RequestBody final CityUpdateDto cityUpdateDto) {
        // Find the item and update the instance
        return cityService.findById(id)
                .map(item -> cityMapper.toModel(cityUpdateDto))
                .flatMap(cityService::update)
                .map(item -> noContent().build());
    }

    @ApiOperation(value = "Get the cities for admin. {access= Admin Role}", authorizations = {@Authorization(value = "JWT")})
    @GetMapping(value = "/cities-details-list", produces = TEXT_EVENT_STREAM_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public Flux<CityShowForAdminDto> getAll() {
        return cityService.findAll()
                .map(CityShowForAdminDto::new);
    }

    @ApiOperation(value = "Get the cities for users. {access= User Role}", authorizations = {@Authorization(value = "JWT")})
    @GetMapping(value = "/cities-list", produces = TEXT_EVENT_STREAM_VALUE)
    @PreAuthorize("hasRole('USER')")
    public Flux<CityShowDto> getCities() {
        return cityService.findAllByVisible(true)
                .map(CityShowDto::new);
    }

    @ApiOperation(value = "Get the subscriptions for user. {access= Any Role}", authorizations = {@Authorization(value = "JWT")})
    @GetMapping(value = "/get-subscriptions", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<CityShowDto> getSubs(@RequestParam("userId") final Integer userId) {
        return cityService.findSubscribedCities(userId).map(CityShowDto::new);
    }

}
