package com.atmos.weatherservicedemo.controllers;

import com.atmos.weatherservicedemo.domain.dtos.*;
import com.atmos.weatherservicedemo.mappers.UserMapper;
import com.atmos.weatherservicedemo.services.CityService;
import com.atmos.weatherservicedemo.services.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/api/user-controller")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;
    private final CityService cityService;
    private final UserMapper userMapper;

    @ApiOperation(value = "Sign On")
    @PostMapping("/register")
    public Mono<AuthResponseDto> register(
            @RequestBody @Validated AuthRequestDto request
    ) {
        return userService.register(userMapper.toModel(request));
    }

    @ApiOperation(value = "Sign In")
    @PostMapping("/login")
    public Mono<AuthResponseDto> authenticate(
            @RequestBody @Validated AuthRequestDto request
    ) {
        return userService.authenticate(userMapper.toModel(request));
    }

    @ApiOperation(value = "Get All Users. {access= Admin Role}", authorizations = {@Authorization(value = "JWT")})
    @GetMapping("/user-list")
    @PreAuthorize("hasRole('ADMIN')")
    public Flux<UserDto> getAllUsers() {
        return userService.getAll().map(userMapper::toDto);
    }

    @ApiOperation(value = "Get exact user's details. {access= Admin Role}", authorizations = {@Authorization(value = "JWT")})
    @SecurityRequirement(name = "authenticated")
    @GetMapping("/user-details")
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<UserDetailsDto> getUserDetails(@RequestParam final Integer id) {
        return userService.getById(id);
    }

    @ApiOperation(value = "Edit user. {access= Admin Role}", authorizations = {@Authorization(value = "JWT")})
    @PutMapping("/edit-user")
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<UserDetailsDto> editUser(@RequestBody final UserDto userDto) {
        return userService.edit(userMapper.toModel(userDto)).map(userMapper::toDetails);
    }

    @ApiOperation(value = "Subscribe to city weather. {access= Any Role}", authorizations = {@Authorization(value = "JWT")})
    @PatchMapping("/subscribe-to-city")
    public Mono<UserDetailsDto> subscribeToCity(@RequestBody final UserSubscribeDto userSubscribeDto) {
        return cityService.findById(userSubscribeDto.getCityId())
                .flatMap(city -> {
                    return userService.subscribe(userSubscribeDto.getUserId(), city);
                });
    }
}
