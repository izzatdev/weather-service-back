package com.atmos.weatherservicedemo.utils;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public interface Utils {
    String dateTimePattern = "HH:mm:ss dd-MM-yyyy";
    Sort DEFAULT_SORT = Sort.by(Sort.Order.by("name"));

    static String getCurrentTimeInString() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern));
    }
}
